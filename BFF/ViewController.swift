//
//  ViewController.swift
//  BFF
//
//  Created by Akylbek Utekeshev on 25.09.2018.
//  Copyright © 2018 Akylbek Utekeshev. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

      @IBOutlet weak var segmentedControll: UISegmentedControl!
  
    @IBOutlet weak var signInFacebook: UIButton!
    @IBOutlet weak var signInInsta: UIButton!
    @IBOutlet weak var signInEmail: UIButton!
    
    @IBOutlet weak var signUPEmail: UIButton!
    @IBAction func segmentedControlAct(_ sender: UISegmentedControl) {
        segmentedControll.changeUnderlinePosition()
        
        if segmentedControll.selectedSegmentIndex == 0 {
            signInEmail.isHidden = false
            signInInsta.isHidden = false
            signInFacebook.isHidden = false
            signUPEmail.isHidden = true
          }
        if segmentedControll.selectedSegmentIndex == 1 {
            signInEmail.isHidden = true
            signInInsta.isHidden = true
            signInFacebook.isHidden = true
            signUPEmail.isHidden = false
        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        segmentedControll.addUnderlineForSelectedSegment()
        signUPEmail.isHidden = true
        }

    
    
}

